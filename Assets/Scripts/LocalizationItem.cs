﻿using System;

[Serializable]
public class LocalizationItem
{
    public string tag;
    public string ru;
    public string en;
}