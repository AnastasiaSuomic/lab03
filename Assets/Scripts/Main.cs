﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class Main : MonoBehaviour
{
    [SerializeField] protected InputField inputName;
    [SerializeField] protected InputField inputSurname;
    [SerializeField] private Button buttonResult;
    [SerializeField] private Text textResult;
    
    
    private void Awake() //сразу после создания объекта
    {
        if (textResult != null)
        {
            textResult.text = "";
        }
    }
    
    private void Start() //на следующий кадр после создания объекта
    {
        if (buttonResult != null)
        {
            buttonResult.onClick.AddListener(OnClicked);
        }
    }

    private void ButtonEnglishOnClicked()
    {
       LocalizationController.instance.SwitchLanguage(Language.en);
    }
    
    private void ButtonRussianOnClicked()
    {
        LocalizationController.instance.SwitchLanguage(Language.ru);
    }

    private void OnDestroy() //при уничтожении объекта
    {
        if (buttonResult != null)
        {
            buttonResult.onClick.RemoveListener(OnClicked);
        }
    }

    protected void ShowToast(string message)
    {
#if UNITY_ANDROID
        UnityNativeToastsHelper.ShowShortText(message);
#endif
    }

    protected void SetTextResult(string result)
    {
        if (textResult != null)
        {
            textResult.text = result;
        }
    }
    
    public void OnClicked()
    {
        SetTextResult("");
        
        if (inputName == null || inputSurname == null) 
            return;
        
        if (inputName.text == "")
        {
            var nameIsEmpty = LocalizationController.instance.GetLabel("emptyName");
            ShowToast(nameIsEmpty);
            return;
        }
        
        if (inputSurname.text == "")
        {
            var surnameIsEmpty = LocalizationController.instance.GetLabel("emptySurname");
            ShowToast(surnameIsEmpty);
            return;
        }

        DoAction();
    }

    protected virtual void DoAction()
    {
        var welcome = LocalizationController.instance.GetLabel("textResult");
        var result = $"{welcome} {inputName.text} {inputSurname.text}";
        SetTextResult(result);

        ShowToast(result);
    }
}
