﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrientationalController : MonoBehaviour
{
    [SerializeField] private GameObject horizontalLayout;
    [SerializeField] private GameObject verticalLayout;
    [SerializeField] private GameObject verticalLayout1;
    [SerializeField] private Button buttonSwitchFragment;
    [SerializeField] private Button buttonNextActivity;
    private ScreenOrientation _previousDeviceOrientation;
    private State _currentState;
    
    
    
    // Start is called before the first frame update
    private void Start()
    {
        ChangeOrientationIfNeed(true);
        buttonSwitchFragment.onClick.AddListener(ButtonSwitchFragmentOnClick);
        buttonNextActivity.onClick.AddListener(ButtonNextActivityOnClick);
    }

    private void ButtonSwitchFragmentOnClick()
    {
        switch (_currentState)
        {
            case State.first : 
                verticalLayout.SetActive(false);
                verticalLayout1.SetActive(true);
                _currentState = State.second;
                break;
            case State.second :
                verticalLayout.SetActive(true);
                verticalLayout1.SetActive(false);
                _currentState = State.first;
                break;
        }
    }
    
    private void ButtonNextActivityOnClick()
    {
        verticalLayout.SetActive(true);
        verticalLayout1.SetActive(true);
    }
    
    private void ChangeOrientationIfNeed(bool forceChange)
    {
        var deviceOrientation = Screen.orientation;
        if (!forceChange && deviceOrientation == _previousDeviceOrientation)
        {
            return;
        }
        var isPortrait = deviceOrientation == ScreenOrientation.Portrait ||
                         deviceOrientation == ScreenOrientation.PortraitUpsideDown;
        if (isPortrait) 
        {
            horizontalLayout.SetActive(false);
            verticalLayout.SetActive(true);
        }
        else
        {
            horizontalLayout.SetActive(true);
            verticalLayout.SetActive(false);
        }

        _previousDeviceOrientation = deviceOrientation;
    }
    
    private void Update()
    {
        ChangeOrientationIfNeed(false);
    }
}
