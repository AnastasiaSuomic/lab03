﻿using System.Linq;

public class ReverseMain : Main
{
        
        protected override void DoAction()
        {
                var welcome = LocalizationController.instance.GetLabel("textResult");
                var reversedName = new string (inputSurname.text.Reverse().ToArray());
                var reversedSurname = new string(inputName.text.Reverse().ToArray());
                var result = $"{reversedName} {reversedSurname}";
                SetTextResult(result);

                ShowToast(result);
        }
}